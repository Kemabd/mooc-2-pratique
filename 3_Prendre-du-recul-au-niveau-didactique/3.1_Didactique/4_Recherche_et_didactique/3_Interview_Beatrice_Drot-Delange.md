Depuis la fin des années 1960 en France, l'informatique est enseignée dans les IUT (institut universitaire de technologie). <a href="https://renoir.uca.fr/" target="_blank">RENOIR-IUT</a> est un projet de recherche financé par l'ANR (agence nationale de la recherche) étudiant l'activité enseignante au prisme des ressources pédagogiques. Les recherches menées par une équipe au sein de ce projet abordent des questions de didactique de l'informatique.

Considérer les phénomènes d'enseignements et d'apprentissages de l'informatique dans les IUT permet de comprendre des phénomènes didactiques dans un système d'enseignement établi depuis longtemps sur l'ensemble du territoire. Pour la recherche en didactique de l'informatique, cela constitue un terrain privilégié permettant de rencontrer des enseignants dont l'activité est éprouvée.

<a href="https://acte.uca.fr/membres/enseignants-chercheurs/beatrice-drot-delange" target="_blank">**Béatrice Drot-Delange**</a>, coordinatrice du projet Renoir-IUT, est professeure des universités en sciences de l'éducation à l'INSPÉ Clermont Auvergne. Elle nous présente comment la recherche appréhende la question de l'ancrage dans le quotidien de situations d'apprentissages et les fonctions didactiques que les projets informatiques sont susceptibles de jouer."

## chapitrage vidéo

1. Présentation de Béatrice Drot-Delange
2. Renoir-IUT un projet de recherche abordant des questions de didactique de l'informatique
3. Élaboration de situations didactiques et activité de programmation basée sur des problèmes
4. Des situations d'apprentissages ancrées dans les pratiques quotidiennes
5. Un enseignement via des projets « ressemblants » à la vie réelle
6. Un modèle didactique permettant d'ancrer dans le quotidien des situations d'apprentissages
