# Ajouter des ressources dans mon espace 

## Téléverser une ressource existante

Nous vous proposons de télécharger le fichier pdf : [exemple_fichier-markdown.pdf](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/1_Bien-Commencer/1.2_Preparer-son-espace-de-travail/exo1/exemple_fichier-markdown.pdf) et de le téléverser dans le dossier _Bien-commencer_ de votre espace de travail.

Dans **votre espace** , une fois positionner au bon endroit :

- Sélectionner `Upload file` :<br>
    ![ajout sol2 etape 1](webide_add_sol2_1.png)
- Déposer votre ressource et réaliser le _commit_, sur la branche _main_<br>
    ![ajout sol2 etape 2](webide_add_sol2_2.png)


## Créer une ressource directement via gitlab

Nous allons créer un fichier markdown.

### Vous connaissez déjà Gitlab

Vous pouvez directement créer le fichier Bien-Commencer/fichier-markdown.md (Attention à ne pas mettre d'espace dans le nom du fichier fichier-markdown.md) et écrire du Markdown pour avoir un rendu qui correspond au [modèle de pdf](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/1_Bien-Commencer/1.2_Preparer-son-espace-de-travail/exo1/exemple_fichier-markdown.pdf), sauver, commiter sous Gitlab. 


### Vous n'avez jamais utilisé Gitlab

Voici les étapes à suivre :

- Allez sur le projet Gitlab mooc-2-ressources en cliquant sur le lien xxx/mooc-2-ressources depuis votre espace Gitlab
- Allez dans le dossier Bien-Commencer (en cliquant sur le dossier Bien-Commencer) et créez le fichier fichier-markdown.md (Bouton "+" / "New file", dans la zone "File name" saisissez "fichier-markdown.md" : Attention à ne pas mettre d'espace dans le nom du fichier)
- Écrire dans ce fichier les lignes de texte qui permettent de reproduire ce [modèle de pdf](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/1_Bien-Commencer/1.2_Preparer-son-espace-de-travail/exo1/exemple_fichier-markdown.pdf)
- Écrire un message de commit dans la zone prévue à cet effet : "Commit message"
- Sélectionner la branche _main_ **pour ne pas en créer une autre** :<br>
        ![ajout sol1 etape 5](webide_add_sol1_5.png)   
- Cliquez sur le bouton "Commit changes"
Le _commit_ permet de valider les modifications apportées à votre espace gitlab, et de ne pas les laisser à l'état de _brouillon_    

_Note_ : La notion de _branche_ correspond à la notion de duplication ou de copie de l'espace de travail. Dans un projet collaboratif, un membre pourrait apporter des modifications au projet mais sur une copie ; ces modifications pourraient ensuite, après validation par l'équipe, être intégrées au travail original (la _branche principale_).


## Quels formats de fichier pour les ressources ?

Nous privilégions les ressources aux formats simples et _relativement_ universels afin de garantir que les ressources puissent être accessibles par le plus grand nombre :

- fichiers markdown et pdf sont vivement conseillés, visualisables directement depuis votre espace gitlab
- si vous avez des vidéos, il faudra que vous assuriez vous-même leur hébergement sur un site de diffusion et **que vous en en proposiez l'accès par un lien hypertexte**.
- on évitera tout document type suite bureautique
