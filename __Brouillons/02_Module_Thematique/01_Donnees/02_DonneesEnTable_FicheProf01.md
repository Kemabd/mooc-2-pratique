# Fiche professeur

**Thématique :** fichiers textes, format CSV.

**Notions liées :** programmation de base ainsi que les listes, les tuples, les dictionnaires, les fichiers

**Résumé de l’activité :** manipuler des fichiers textes avec Python, manipuler des fichiers de données type CSV avec Python et avec un tableur.

**Objectifs :** comprendre la notion de données en tables, la notion de fichier texte. Savoir manipuler dans un petit script Python des objets de cette nature.

**Niveau :** il s'agit clairement d'un contenu de classe de 1re, ayant déjà bien manipulé les types construits.

**Auteur :** Sébastien Hoarau seb.hoarau@univ-reunion.fr

**Durée de l’activité :** 2H 

**Forme de participation :** individuelle en autonomie, groupe classe discussion

**Matériel nécessaire :** ordinateur avec un interprète Python installé, un tableur, une connexion à internet.

**Préparation :** s'assurer des pré-requis ; les élèves doivent savoir utiliser une liste, un tuple, un dictionnaire.

---

La séance proposée est une série de questions / manipulations via un notebook. A mi-séance environ, une question est posée donnant lieu à une pause générale et une discussion. Cette première séance est une grosse introduction à une seconde qui va se focaliser sur le traitement des données en table.

## Déroulement

Je les laisse immédiatemment ouvrir le notebook présent sur leurs machines.

**Découverte** : les questions 1 à 4 permettent de manipuler l'ouverture d'un fichier texte en lecture, de récupérer les lignes (chaînes de caractères) dans une liste, de transformer ces lignes en d'autres données, de créer de nouvelles données qu'on va venir écrire dans le fichier après avoir créé une chaîne de caractères.

**Question pause** : je demande aux élèves de me résumer la chaîne de traitement : ouverture, lecture, transformation, création, transormation, écriture.


**Conclusion et préparation de l'activité 2** : les questions 5 à 7 permettent de voir qu'il est plus aisé de manipuler un très gros fichier avec un programme Python qu'avec un tableur. On prépare la prochaine activité : le traitement de données en tables.



