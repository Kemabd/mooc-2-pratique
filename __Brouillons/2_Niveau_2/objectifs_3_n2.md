## Accompagner l'individu et le collectif
### Niveau 2

- Proposer une activité de remédiation et d'approfondissement à partir d'une idée
- Proposer un projet en précisant :
    - les connaissances travaillées
    - le format pédagogique (durée, seul, binôme, groupe...)
    - les étayages pour assurer une progression des élèves
    - le calendrier (points d'étapes, rendus intermédiaires...)
